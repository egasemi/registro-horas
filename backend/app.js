const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const cors = require('cors');
const Persona = require('./models/Persona')
const Horario = require('./models/Horario')
const app = express();

// conectar a la db

mongoose.connect(process.env.MONGO_URI || 'mongodb://localhost/horarios',{
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})
    .then(db => console.log('db conectada'))
    .catch(err => console.log(err));

// config

app.set('port', process.env.PORT || 3000);
var corsOptions = {
    origin: process.env.ORIGIN || 'http://localhost:8000'
}

//middlewares

app.use(morgan('dev'));
app.use(express.json());
app.use(cors(corsOptions))

// rutas

app.get('/', (req, res) => {
    try {
        res.send(true)
    } catch (error) {
        res.send({message: "danger"})
        console.log(error)
        
    }
})


app.get('/personas', async(req, res) =>{
    var personas = await Persona.find()
    res.send(personas)
})
app.get('/horarios', async(req, res) =>{
    var horarios = await Horario.find()
    .populate('persona')
    res.send(horarios)
})

app.post('/agregar/:tipo', async (req, res) => {
    try {
        var { tipo } = req.params
        switch (tipo) {
            case 'Persona':
                var registro = new Persona(req.body)
                break;

            case 'Horario':
                var registro = new Horario(req.body)
                await registro.populate('persona').execPopulate()
                break;

            default:
                break;
        }
        registro.save()
        res.send({message: 'success',registro})
    } catch (error) {
        console.log(error)
        res.send({message: 'danger'})
    }
})

app.delete('/borrar/:tipo/:id', async (req, res) => {
    try {
        const { tipo, id } = req.params
        switch (tipo) {
            case 'Persona':
            await Persona.findByIdAndDelete(id)
            await Horario.deleteMany({persona: id})
                break;

            case 'Horario':
            await Horario.findByIdAndDelete(id)
                break;

            default:
                break;
        }
        res.send({message: 'success'})
    } catch (error) {
        console.log(error)
        res.send({message: 'danger'})
    }
})

app.post('/editar/:tipo/:id', async (req, res) => {
    try {
        const { tipo, id } = req.params
        switch (tipo) {
            case 'Persona':
            var registro = await Persona.findOneAndUpdate({ _id:id }, req.body, {new: 'true'})
                break;

            case 'Horario':
            var registro = await Horario.findOneAndUpdate({_id: id}, req.body, {new: 'true'})
            await registro.populate('persona').execPopulate()
                break;
        
            default:
                break;
        }
        await registro.save()
        res.send({message: 'success', registro})
    } catch (error) {
        console.log(error)
        res.send({message: 'danger'})
    }
})

// arranque server

app.listen(app.get('port'), () => {
    console.log(`server en puerto ${app.get('port')}`)
})