
const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const personaSchema = new Schema({
    nombre: String
},{
    timestamps: true,
    versionKey: false
})

module.exports = mongoose.model('persona', personaSchema)