const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const horarioSchema = new Schema({
    persona: {
        type: Schema.Types.ObjectId,
        ref: 'persona'
    },
    tipo_horario: String,
    horario: Date
},{
    timestamps: true,
    versionKey: false
})

module.exports = mongoose.model('horario', horarioSchema)