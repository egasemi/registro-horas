# Registro Horarios

Proyecto de práctica de un CRUD básico con el sistema MEVN (MongoDB, Express.js, Vue.js, Node.js) donde se pueden registrar personas y horarios de trabajo para saber cuántas horas trabajó cada persona a traves de un api rest. Se puede probar en esta [demo](https://egasemi.gitlab.io/registro-horas).

## Implementación local
### Enviroment
#### Versiones
    Node.js v14.16.1
    npm v6.14.12
    MongoDB v3.6.8

#### Run DB
    mongod

#### Run backend
    npm install
    npm run dev

#### Run frontend
    cd frontend
    python3 -m http.server // para python 3.~
    python -m SimpleHTTPServer // para python 2.~


Los tres comandos tienen que estar corriendo al mismo tiempo, osea que deberías tener tres consolas en paralelo.

Por defecto la api corre en http://localhost:3000 y la app en http://localhost:8000
