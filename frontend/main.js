const App = {
    data() {
        return {
            url: 'https://registro-horas.herokuapp.com/',
            form: 'horario',
            nuevaPersona: '',
            persona: '',
            body: {},
            tipo_horario: '',
            nuevoHorario: '',
            horario: '',
            personas: [],
            horarios : [],
            editar: false
        }
    },
    methods: {
        agregarPersona: async function(){

            var persona = {
                nombre: this.nuevaPersona
            }
            if (this.editar) {
                var info = { nombre: this.nuevaPersona }
                var response = await axios.post(`${this.url}editar/Persona/${this.body.data._id}`,info)
                console.log(response.data.message)
                var indice = this.body.indice
                this.personas[indice].nombre = this.nuevaPersona

                this.editar = !this.editar

            } else {
                var response = await axios.post(`${this.url}agregar/Persona`, persona)
                this.personas.push(response.data.registro)
                console.log(response.data.message)
            }
            this.nuevaPersona = ''

        },
        editarPersona: function(index){
            this.nuevaPersona = this.personas[index].nombre
            this.body = {
                data: this.personas[index],
                indice: index
            }
            this.editar = !this.editar
        },
        eliminarPersona: async function(index){
            var id = this.personas[index]._id
            var response = await axios.delete(`${this.url}borrar/Persona/${id}`)
            console.log(response.data.message)
            this.personas.splice(index,1)
        },

        agregarHorario: async function(){

            var horario = {
                persona: this.persona,
                tipo_horario: this.tipo_horario,
                horario: this.horario
            }
            if (this.editar) {
                var response = await axios.post(`${this.url}editar/Horario/${this.body.data._id}`, horario)
                console.log(response.data.registro.message)
                this.horarios[this.body.indice] = response.data.registro
                this.editar = !this.editar

            } else {
                var response = await axios.post(`${this.url}agregar/Horario`, horario)
                this.horarios.push(response.data.registro)
                console.log(response.data.message)
            }
            this.persona = ''
            this.tipo_horario = ''
            this.horario  = ''
        },

        editarHorario: function(index){
            this.editar = !this.editar

            this.persona = this.horarios[index].persona._id

            this.tipo_horario = this.horarios[index].tipo_horario

            var d = new Date(this.horarios[index].horario)

            var ano = d.getFullYear()
            var mes = ("0"+(d.getMonth()+1)).slice(-2)
            var dia = ("0" + d.getDate()).slice(-2)
            var hora = d.toLocaleTimeString()

            this.horario = `${ano}-${mes}-${dia}T${hora}`

            this.body = {
                data: this.horarios[index],
                indice: index
            }

        },
        eliminarHorario: async function(index){
            var response = await axios.delete(`${this.url}borrar/Horario/${this.horarios[index]._id}`)
            console.log(response.data.message)
            this.horarios.splice(index,1)
        },
        horaActual: function(){
            if (!this.editar) {
                var d = new Date()
                var ano = d.getFullYear()
                var mes = ("0"+(d.getMonth()+1)).slice(-2)
                var dia = ("0" + d.getDate()).slice(-2)
                var hora = d.toLocaleTimeString()
    
                this.horario = `${ano}-${mes}-${dia}T${hora}`
            }

        }
    },
    async created() {
        try {
            var prod = await axios.get(this.url)
            var origin = prod.data
        } catch (error) {
            var origin = false
        }

        if (!origin) {
            this.url = 'http://localhost:3000/'
        }

        
        var datosHorarios = await axios.get(`${this.url}horarios`);
        var datosPersonas = await axios.get(`${this.url}personas`);

        if (datosHorarios === null || datosHorarios.length < 0) {
            this.horarios = []
        } else {
            this.horarios = datosHorarios.data
        }
        if (datosPersonas === null || datosPersonas.length < 0) {
            this.personas = []
        } else {
            this.personas = datosPersonas.data
        }
    }
}

Vue.createApp(App).mount('#app')
